# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit qt4-r2 git-2

DESCRIPTION="A simple pump.io client written with Qt"
HOMEPAGE="http://sjoberg.fi/software/pumpa.html"
EGIT_REPO_URI="https://git.gitorious.org/pumpa/pumpa.git"
EGIT_COMMIT="tags/v${PV}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="debug spell"
LANGS="en fr it de nvi"

DEPEND="
	dev-qt/qtcore
	dev-qt/qtgui
	dev-qt/qtdbus
	dev-libs/qjson
	dev-libs/kqoauth
	spell? ( app-text/aspell )
"
RDEPEND="${DEPEND}"

src_configure () {
	if use debug; then
		eqmake4 "${S} CONFIG+=debug"
	else
		eqmake4 "${S}"
	fi
}
