# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{2_6,2_7} pypy )

inherit git-2 distutils-r1


DESCRIPTION="A command line pump.io client."
HOMEPAGE="https://github.com/xray7224/p/"
EGIT_REPO_URI="https://github.com/xray7224/p.git"
EGIT_COMMIT="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	=dev-python/pypump-9999
	>=dev-python/click-2.0
	>=dev-python/pytz-2014.4
	>=dev-python/html2text-3.02
"
RDEPEND="${DEPEND}"
