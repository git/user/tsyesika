# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{2_6,2_7,3_3,3_4} pypy )


inherit git-2 distutils-r1

DESCRIPTION="A pythonic library for the federated pump.io protocol."
HOMEPAGE="http://pypump.org"
EGIT_REPO_URI="https://github.com/xray7224/pypump.git"
EGIT_COMMIT="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-python/requests
	dev-python/requests-oauthlib
	dev-python/python-dateutil
"
RDEPEND="${DEPEND}"


