# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{3_3,3_4} )


inherit git-2 distutils-r1

DESCRIPTION="A pub-sub framework written in python for processing data from multiple sources."
HOMEPAGE="https://github.com/Stebalien/overkill"
EGIT_REPO_URI="https://github.com/Stebalien/overkill.git"
EGIT_COMMIT="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-python/pyinotify
"
RDEPEND="${DEPEND}"


