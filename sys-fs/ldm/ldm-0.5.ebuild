# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2

DESCRIPTION="Lightweight Device Mounter"
HOMEPAGE="https://github.com/LemonBoy/ldm"
EGIT_REPO_URI="https://github.com/LemonBoy/ldm.git"
EGIT_COMMIT="${VN}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
